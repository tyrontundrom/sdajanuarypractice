package Zadania;

import java.util.Scanner;

public class Round {
    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        System.out.println("Wprowadź średnicę");
        float srednica = in.nextFloat();
        float pi = 3.14F;
        float obwod = pi * srednica;
        System.out.println("Srednica okręgu to " + obwod);
    }
}
