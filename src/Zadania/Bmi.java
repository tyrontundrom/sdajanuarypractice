package Zadania;

import java.util.Scanner;

public class Bmi {
    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        System.out.println("Podaj masę w kilogramach");
        float masa = in.nextFloat();
        System.out.println("Podaj wzrost w centymetrach");
        int wzrost = in.nextInt();
        wzrost /= 100.0;
        double wzrost2 = (double) Math.pow(wzrost, 2);
        double bmi = (double) masa / wzrost2;

        if (bmi < 18.75 || bmi > 24.9) {
            System.out.println("BMI wynosi " + bmi + " i jest nieoptymalne");
        } else {
            System.out.println("BMI wynosi " + bmi + " jest optymalne");
        }
    }
}
