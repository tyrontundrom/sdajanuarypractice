package Zadania;

import java.util.Scanner;

public class Letters {
    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        System.out.println("Podaj pierwszą literę");
        String letter1 = in.nextLine();
        System.out.println("Podaj drugą literę");
        String letter2 = in.nextLine();
        char charact1 = letter1.charAt(0);
        char charact2 = letter2.charAt(0);
        if (charact1 > charact2) {
            System.out.print("Między znakiem \'" + charact1 + "\' a \'" + charact2 + "\' znajduje się ");
            System.out.println(charact1 - charact2 + " znaków");
        } else if (charact2 > charact1) {
            System.out.print("Między znakiem \'" + charact1 + "\' a \'" + charact2 + " znajduje się ");
            System.out.println(charact2 - charact1 + " znaków");
        }
    }
}
