package Zadania;

import java.util.ArrayList;
import java.util.Scanner;

public class Starczy {
    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        System.out.println("Wpisz poprawne słowo");
        String word = in.nextLine();
        ArrayList <String> words = new ArrayList<String>();
        words.add(word);

        while (!(word.equals("Starczy"))) {
            if (word.equals("")) {
            System.out.println("Nie podano żadnego tekstu");
        }
            System.out.println("Wpisz poprawne słowo");
            word = in.nextLine();
            words.add(word);
        }
        words.remove("Starczy");
        System.out.println(words);
        String theLongestElement = words.get(0);
        for (String element : words) {
            if (element.length() > theLongestElement.length()) {
                theLongestElement = element;
            }
        }
        System.out.println(theLongestElement);
    }
}
