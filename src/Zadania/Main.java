package Zadania;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {

        int maxStrophes = 0;

        Author author1 = new Author("Kowalski", "Polska");
        Author author2 = new Author("Wayne", "Amerykanin");
        Author author3 = new Author("Shwarz", "Niemiec");

        Poem poem1 = new Poem(author1, 30);
        Poem poem2 = new Poem(author2, 25);
        Poem poem3 = new Poem(author3, 7);

        List<Poem> poemList = new ArrayList<>();
        poemList.add(poem1);
        poemList.add(poem2);
        poemList.add(poem3);

        for (Poem poem : poemList) {
            if (poem.getStropheNumbers() > maxStrophes) {
                maxStrophes = poem.getStropheNumbers();
            }
        }
        System.out.println(maxStrophes);
    }
}
