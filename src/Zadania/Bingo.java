package Zadania;

import java.util.Random;
import java.util.Scanner;

public class Bingo {
    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        int random = new Random().nextInt(100);
        //System.out.println(random);
        System.out.println("Podaj liczbe od 0 do 100");
        int numbUser = in.nextInt();

        while (!(numbUser == random)) {
            if (numbUser > random) {
                System.out.println("Za dużo");
            } else {
                System.out.println("Za mało");
            }
            numbUser = in.nextInt();
        }
        System.out.println("Bingo!");
    }
}
