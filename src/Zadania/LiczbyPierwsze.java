package Zadania;

import java.util.Scanner;

public class LiczbyPierwsze {
    public static void main(String[] args) {

        int number = getNumber();

        for (int i = 2; i < number; i++) {
            if ((i % 2 != 0) && (i % 3 != 0) && (i % 5 != 0)) {
                System.out.println(i);
            } else if (i == 2 || i == 3 || i == 5) {
                System.out.println(i);
            }
        }
    }

    private static int getNumber() {
        Scanner in = new Scanner(System.in);
        System.out.println("Podaj liczbę");
        int number = in.nextInt();
        return number;
    }
}
